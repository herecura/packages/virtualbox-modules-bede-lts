# vim:set ft=sh et:
# Maintainer: Bartłomiej Piotrowski <nospam@bpiotrowski.pl>
# Contributor: Ionut Biru <ibiru@archlinux.org>
# Contributor: Sébastien Luttringer <seblu@aur.archlinux.org>

pkgbase=virtualbox-modules-bede-lts
pkgname=('virtualbox-modules-bede-lts-host')
pkgver=7.1.6
_current_linux_version=6.12.18
_next_linux_version=6.13
pkgrel=12
arch=('x86_64')
url='http://virtualbox.org'
license=('GPL')
makedepends=(
    "linux-bede-lts=$_current_linux_version"
    "linux-bede-lts-headers=$_current_linux_version"
    "virtualbox-host-dkms=$pkgver"
)
source=('modules-load-virtualbox-bede-lts')
sha512sums=('80bf2c8a402ab066fb70c39a241fa92494c07616fe347bd2ed35b68ddfddab1ceed6322493848f28400fc4706c631e7376646aac0bb572b686bf98aa6d8a8991')


package_virtualbox-modules-bede-lts-host() {
    pkgdesc="Kernel host modules for VirtualBox (linux-bede-lts)"
    license=('GPL')
	depends=(
        "linux-bede-lts=$_current_linux_version"
    )
    provides=('VIRTUALBOX-HOST-MODULES')

    local kernver="$(</usr/src/linux-bede-lts/version)"
    local extradir="/usr/lib/modules/$kernver/extramodules"

    # output dkms log for easier debugging
    if [[ -f "/var/lib/dkms/vboxhost/${pkgver}_OSE/build/make.log" ]]; then
        cat "/var/lib/dkms/vboxhost/${pkgver}_OSE/build/make.log"
    fi
    # when dkms was used
    cd "/var/lib/dkms/vboxhost/${pkgver}_OSE/$kernver/$CARCH/module"
    # when build is used
    #cd dkms/vboxhost/${pkgver}_OSE/$_kernver/$CARCH/module
    install -dm755 "${pkgdir}${extradir}/$pkgname"
    install -Dm644 * "${pkgdir}${extradir}/$pkgname/"
    find "${pkgdir}" -name '*.ko' -exec xz {} +

    # install config file in modules-load.d for out of the box experience
    install -Dm644 "$srcdir/modules-load-virtualbox-bede-lts" \
        "$pkgdir/usr/lib/modules-load.d/virtualbox-modules-bede-lts-host.conf"
}
